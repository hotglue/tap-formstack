"""Stream type classes for tap-formstack."""

from typing import Optional

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_formstack.client import formstackStream


class FormsStream(formstackStream):
    name = "forms"
    path = "/form"
    primary_keys = ["id"]
    records_jsonpath = "$.forms.[*]"
    replication_key = "updated"
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("created", th.DateTimeType),
        th.Property("db", th.StringType),
        th.Property("deleted", th.StringType),
        th.Property("folder", th.StringType),
        th.Property("language", th.StringType),
        th.Property("name", th.StringType),
        th.Property("num_columns", th.StringType),
        th.Property("progress_meter", th.StringType),
        th.Property("submissions", th.StringType),
        th.Property("submissions_unread", th.StringType),
        th.Property("updated", th.DateTimeType),
        th.Property("viewkey", th.StringType),
        th.Property("views", th.StringType),
        th.Property("submissions_today", th.IntegerType),
        th.Property("last_submission_id", th.StringType),
        th.Property("last_submission_time", th.DateTimeType),
        th.Property("url", th.StringType),
        th.Property("encrypted", th.BooleanType),
        th.Property("thumbnail_url", th.StringType),
        th.Property("submit_button_title", th.StringType),
        th.Property("inactive", th.BooleanType),
        th.Property("timezone", th.StringType),
        th.Property("should_display_one_question_at_a_time", th.BooleanType),
        th.Property("can_access_1q_feature", th.BooleanType),
        th.Property("is_workflow_form", th.BooleanType),
        th.Property("is_workflow_published", th.BooleanType),
        th.Property("has_approvers", th.BooleanType),
        th.Property("edit_url", th.StringType),
        th.Property("data_url", th.StringType),
        th.Property("summary_url", th.StringType),
        th.Property("rss_url", th.StringType),
        th.Property("permissions", th.IntegerType),
        th.Property("can_edit", th.BooleanType),
    ).to_dict()

    def get_child_context(self, record: dict, context: Optional[dict]) -> dict:
        return {"form_id": record["id"]}


class FormsSubmissionsStream(formstackStream):
    name = "submissions"
    primary_keys = ["id"]
    records_jsonpath = "$.submissions.[*]"
    parent_stream_type = FormsStream
    path = "/form/{form_id}/submission"
    replication_key = "timestamp"
    ignore_parent_replication_key = True
    schema = th.PropertiesList(
        th.Property("id", th.StringType),
        th.Property("timestamp", th.DateTimeType),
        th.Property("user_agent", th.StringType),
        th.Property("remote_addr", th.StringType),
        th.Property("payment_status", th.StringType),
        th.Property("latitude", th.StringType),
        th.Property("longitude", th.StringType),
        th.Property("read", th.StringType),
        th.Property("data", th.CustomType({"type": ["object", "string"]})),
    ).to_dict()
