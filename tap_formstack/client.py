"""REST client handling, including formstackStream base class."""

from pathlib import Path
from typing import Any, Dict, Optional

import requests
from pendulum import parse
from singer_sdk.streams import RESTStream

from tap_formstack.auth import OAuth2Authenticator


class formstackStream(RESTStream):
    """formstack stream class."""

    url_base = "https://www.formstack.com/api/v2"
    _page_size = 100

    @property
    def authenticator(self) -> OAuth2Authenticator:
        """Return a new authenticator object."""
        return OAuth2Authenticator(
            self, self._tap.config_file, "https://www.formstack.com/api/v2/oauth2/token"
        )

    @property
    def http_headers(self) -> dict:
        """Return the http headers needed."""
        headers = {}
        if "user_agent" in self.config:
            headers["User-Agent"] = self.config.get("user_agent")
        return headers

    def get_next_page_token(
        self, response: requests.Response, previous_token: Optional[Any]
    ) -> Optional[Any]:
        """Return a token for identifying next page or None if no more pages."""

        response = response.json()
        total = response.get("total")
        previous_token = previous_token or 1

        if (previous_token * self._page_size) < int(total):

            return previous_token + 1

        return None

    def get_starting_time(self, context):
        start_date = self.config.get("start_date")
        if start_date:
            start_date = parse(self.config.get("start_date"))
        rep_key = self.get_starting_timestamp(context)
        return rep_key or start_date

    def get_url_params(
        self, context: Optional[dict], next_page_token: Optional[Any]
    ) -> Dict[str, Any]:
        """Return a dictionary of values to be used in URL parameterization."""
        params: dict = {}
        # Set url parameters
        params["data"] = False
        params["per_page"] = self._page_size
        params["expand_data"] = False
        if next_page_token:
            params["page"] = next_page_token

        if self.replication_key:
            start_date = self.get_starting_time(context)
            if start_date:
                params["min_time"] = start_date.strftime("%Y-%m-%d %H:%M:%S")

        return params
