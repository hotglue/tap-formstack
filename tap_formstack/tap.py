"""formstack tap class."""

from typing import List

from singer_sdk import Stream, Tap
from singer_sdk import typing as th

from tap_formstack.streams import FormsStream, FormsSubmissionsStream

STREAM_TYPES = [FormsStream, FormsSubmissionsStream]


class Tapformstack(Tap):
    """formstack tap class."""

    name = "tap-formstack"

    def __init__(
        self,
        config=None,
        catalog=None,
        state=None,
        parse_env_config=False,
        validate_config=True,
    ) -> None:
        self.config_file = config[0]
        super().__init__(config, catalog, state, parse_env_config, validate_config)

    config_jsonschema = th.PropertiesList(
        th.Property(
            "client_id",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="Client ID for the API service",
        ),
        th.Property(
            "client_secret",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="Client Secret for the API service",
        ),
        th.Property(
            "refresh_token",
            th.StringType,
            required=True,
            secret=True,  # Flag config as protected.
            description="Refresh token for the API service",
        ),
        th.Property(
            "expires_in",
            th.IntegerType,
            required=True,
            secret=True,  # Flag config as protected.
            description="Expiration time in seconds for the API service",
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://www.formstack.com/api/v2",
            description="The url for the API service",
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]


if __name__ == "__main__":
    Tapformstack.cli()
